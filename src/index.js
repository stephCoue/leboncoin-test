import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import GalleryItem from "./GalleryItem";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={App} />
      <Route path="/gallery/:id" component={GalleryItem} />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
registerServiceWorker();
