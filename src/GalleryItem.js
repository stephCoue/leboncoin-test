import React, { Component } from "react";

const GalleryItem = ({ location }) => {
  return (
    <div>
      <h3>{location.state.title}</h3>
      <img src={location.state.url} alt={location.state.title} />
    </div>
  );
};

export default GalleryItem;
