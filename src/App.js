import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Link } from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { photosList: [] };
    this.setPhotoList();
  }
  getPhotos = async () =>
    await (await fetch("https://jsonplaceholder.typicode.com/photos")).json();

  setPhotoList = () => {
    this.getPhotos().then(data => this.setState({ photosList: data }));
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <ul>
          {this.state.photosList.map(item => (
            <li key={item.id}>
              <Link to={{ pathname: `/gallery/${item.id}`, state: item }}>
                <img src={item.thumbnailUrl} alt={item.title} />
              </Link>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default App;
